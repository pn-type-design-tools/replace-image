# ———————————————————————————————
# ———— REPLACE BG IMAGE (v4) ————
# ———————— for Robofont —————————
# —————— by @pnowelldesign ——————
# ———————————————————————————————



useSpecificLayer = 'images'

# ———————————————————————————————
# TIPS:
# useSpecificLayer
#       This variable allows you to specify a layer to put images into.
#       For example, always use the layer 'background' or 'images'
#       To always use the current layer, write:  useSpecificLayer = ''
# ———————————————————————————————






# —————————— DON'T TOUCH BELOW ——————————


from mojo import roboFont
from mojo.UI import Message, GetFile

rofo = roboFont
rfVersion = float( rofo.version )
# print( 'You are using Rofo version:', rfVersionFloat ) # DEBUG
thisFont = CurrentFont()
thisLayer = CurrentLayer()
thisGlyph = CurrentGlyph()
imgTransform = ( 1, 0, 0, 1, 0, 0 ) # default fallback
imgColor = (0, 0, 0, 0.5) # default fallback

pluginName = 'Replace Image'
userMessages = {
    'chooseImage' :
        'Choose a replacement background image for this glyph ({glyphName})'.format( glyphName = thisGlyph.name ),
    'noExistingImage' :
        '''Could not find an existing image. Just going to insert your image with a default size & position.''',
    'incorrectLayerName' :
        '''You specified a target layer named `{targetLayerName}`, but it wasn’t found in this font. No problem—the image will be placed in your current layer, instead.\n\nTo prevent this from happening in the future, open the {pluginName} script and check that the layer name is spelled correctly. Capitalization matters. To open the script, hold down the option key when choosing {pluginName} from the Scripts menu. Then edit the value of the useSpecificLayer variable. Make sure the value is wrapped in single straight quotation marks.\n\nIf you’d prefer to put images in the current layer you’re working with, rather than always using a specific layer, you can write:\nuseSpecificLayer = '' '''.format( targetLayerName = useSpecificLayer, pluginName = pluginName ),
    'failedPNGConversion' :
        '''Failed to convert your image to a PNG. Unfortunately, the UFO format only supports PNG images. You might try converting your image to PNG, then re-running this plugin to see if that works.'''
}

def getImageAttributes( targetLayer ) :
    global imgTransform
    global imgColor
    # First, check for the presence of an image
    try:
        img = targetLayer.image
        # Get the transformation (includes position, scale, rotation)
        imgTransform = img.transformation
        # Get color (includes alpha/opacity)
        imgColor = img.color
    except :
        print( userMessages['noExistingImage'] )

def setImageAttributes( targetLayer ) :
    img = targetLayer.image
    # Set the transformation (includes position, scale, rotation)
    img.transformation = imgTransform
    # Get color (includes alpha/opacity)
    img.color = imgColor
    thisGlyph.update()

def checkIfPNG( fileName ) :
    # check if last 4 chars include 'png'
    fileNameEnding = fileName[-4:]
    if 'png' in fileName :
        return True
    else :
        return False

def convertToPNG( filePath ) :
    # Returns PNG image data (not an image path)
    # If before Robofont 3.3, use the old PNG converter
    if rfVersion < 3.3 :
        from mojo.tools import imagePNGData
        return imagePNGData( filePath )
    # If 3.3 or later, use the new Robofont mojo tools converter
    else :
        import os
        from lib.tools.misc import imagePNGData
        return imagePNGData( filePath )

def chooseImageLayer( layerName ) :
    global targetLayer
    # Returns an RGlyph with the chosen layer selected
    # If a target layer has been specified by the user...
    if len( layerName ) > 0 :
        # Check for the presence of that layer name in the font
        try :
            # If the layer exists, return it
            checkForLayer = thisFont.getLayer( layerName ) # proceeds if found
            targetLayer = thisGlyph.newLayer( layerName )
            # print( targetLayer ) # DEBUG
            return targetLayer
        except :
            # If no layer exists, alert the user...
            Message( pluginName ,
                informativeText = userMessages['incorrectLayerName'] )
            # ... and proceed with the current layer
            # print( thisGlyph ) # DEBUG
            targetLayer = thisGlyph
            return targetLayer
    else :
        # If a target layer has NOT been specified, proceed with the current layer
        targetLayer = thisGlyph
        return targetLayer

def setImage( filePath, targetLayer ) :
    # If it appears to be a PNG, no need to convert
    if checkIfPNG( filePath ) :
        targetLayer.addImage( filePath )
    # If not a PNG, try to convert it
    else :
        # print( '(Just FYI, RoFo prefers a PNG image. Will try to convert it...)' ) # DEBUG
        try :
            convertedPNG = convertToPNG( filePath )
            targetLayer.addImage( data=convertedPNG )
            thisGlyph.update()
        except :
            # If the conversion fails, alert the user...
            Message( pluginName ,
                informativeText = userMessages['failedPNGConversion'] )

def getReplacementImage() :
    # Returns a file path to the image, or None if canceled
    global replacementImageFilePath
    replacementImageFilePath = GetFile(
        message = userMessages['chooseImage'],
        fileTypes = ['PNG','JPG', 'JPEG', 'TIFF'] )

# Run the Functions!
getReplacementImage()
if replacementImageFilePath :
    # print('got an image: ', replacementImageFilePath) # DEBUG
    chooseImageLayer( useSpecificLayer )
    getImageAttributes( targetLayer )
    thisGlyph.prepareUndo('Replace Image')
    setImage( replacementImageFilePath, targetLayer )
    setImageAttributes( targetLayer )
    thisGlyph.performUndo()
