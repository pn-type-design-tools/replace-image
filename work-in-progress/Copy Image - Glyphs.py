#MenuTitle: Copy Image from Another Glyph
# -*- coding: utf-8 -*-
__doc__= ''' Copy another glyph’s background image into your current glyph. Will match the copied image’s size, position, etc.'''

# ———————————————————————————————
# —————— COPY BG IMAGE (v1) —————
# ————————— for Glyphs ——————————
# —————— by @pnowelldesign ——————
# ———————————————————————————————


thisLayer = Glyphs.font.selectedLayers[0]
thisGlyph = thisLayer.parent
imgTransform = ( 1, 0, 0, 1, 0, 0 ) # default fallback
imgColor = 50 # default fallback

pluginName = 'Copy Image'
userMessages = {
    'noSourceImage' :
        '''The glyph you chose does not have an image to copy. Please choose a different glyph...'''
}

def setImageObject( imageData, targetLayer ) :
    # imageData already contains transform and alpha data too
    targetLayer.backgroundImage = imageData
    # Ensure the pasted image is unlocked
    targetLayer.backgroundImage.locked = False

def chooseSourceGlyph() :
    # Choose glyphs from Glyphs App's nice component picking window
    # Returns a glyph, if it has an image
    GSSelectGlyphsDialogController = objc.lookUpClass("GSSelectGlyphsDialogController")
    selectGlyphPanel = GSSelectGlyphsDialogController.alloc().init()
    selectGlyphPanel.setTitle_("Find Glyphs")

    Master = Font.masters[0] # Pick with master you are interested in, e.g., currentTab.masterIndex
    selectGlyphPanel.setMasterID_(Master.id)
    selectGlyphPanel.setContent_(list(Font.glyphs))
    PreviousSearch = Glyphs.defaults["PickGlyphsSearch"]
    if PreviousSearch and len(PreviousSearch) > 0:
    	selectGlyphPanel.setSearch_(PreviousSearch)

    if selectGlyphPanel.runModal():
    	Glyphs.defaults["PickGlyphsSearch"] = selectGlyphPanel.glyphsSelectSearchField().stringValue()
    	#for glyph in selectGlyphPanel.selectedGlyphs():
        chosenGlyphs = selectGlyphPanel.selectedGlyphs()
        return chosenGlyphs[0]

def runAll() :
    # Choose the source glyph
    sourceGlyph = chooseSourceGlyph()
    if sourceGlyph :
        # If a glyph was chosen (rather than clicking Cancel),
        # Get the primary layer for that glyph
        sourceLayer = sourceGlyph.layers[0]
        try :
            # If the chosen glyph has an image, copy it to the current glyph
            img = sourceLayer.backgroundImage
            setImageObject( img, thisLayer )
        except :
            # If the chosen glyph does NOT have an image, alert the user
            Message(
        		title = pluginName,
        		message = userMessages['noSourceImage'],
        		OKButton = "Choose another glyph"
    		)
            # ... then re-open the glyph chooser (start over)
            runAll()
# Kick off
runAll()
