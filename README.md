# Replace Image
Choose a file to replace the glyph’s background image, while matching the size, position, etc. of the current image (even if the current image is missing.)

## Improvement Ideas
- Better undo support (without needing to change layers)
- Store the last-used directory (folder) for that font, then open the file chooser window to that folder.
