#MenuTitle: Replace Image
# -*- coding: utf-8 -*-
__doc__=''' Choose a file to replace the glyph’s background image, while matching the size, position, etc. of the current image (even if the current image is missing.)'''

# ———————————————————————————————
# ———— REPLACE BG IMAGE (v4) ————
# ————————— for Glyphs ——————————
# —————— by @pnowelldesign ——————
# ———————————————————————————————


thisLayer = Glyphs.font.selectedLayers[0]
thisGlyph = thisLayer.parent
imgTransform = ( 1, 0, 0, 1, 0, 0 ) # default fallback
imgColor = 50 # default fallback

pluginName = 'Replace Image'
userMessages = {
    'chooseImage' :
        'Choose a replacement background image for this glyph ({glyphName})'.format( glyphName = thisGlyph.name ),
    'noExistingImage' :
        '''Could not find an existing image. Just going to insert your image with a default size & position.'''
}

def getImageAttributes( targetLayer ) :
    global img
    global imgTransform
    global imgColor
    # First, check for the presence of an image
    try :
        img = targetLayer.backgroundImage
        # Get the transformation (x scale, x skew, y skew, y scale, x pos, y pos)
        imgTransform = img.transform
        # Get the alpha (opacity, on a 0-100 scale)
        imgColor = img.alpha
    except :
        print( userMessages['noExistingImage'] )

def setImageAttributes( targetLayer ) :
    img = targetLayer.backgroundImage
    # Set the transformation (x scale, x skew, y skew, y scale, x pos, y pos)
    img.transform = imgTransform
    # Get color (includes alpha/opacity)
    img.alpha = imgColor

def setImage( filePath, targetLayer ) :
    targetLayer.backgroundImage = GSBackgroundImage( replacementImageFilePath )

def getReplacementImage() :
    # Returns a file path to the image, or None if canceled
    global replacementImageFilePath
    replacementImageFilePath = GetOpenFile(
        message = userMessages['chooseImage'],
        filetypes = ['PNG','JPG', 'JPEG', 'TIFF'] )

# Run the Functions!
getReplacementImage()
if replacementImageFilePath :
    getImageAttributes( thisLayer )
    setImage( replacementImageFilePath, thisLayer )
    setImageAttributes( thisLayer )
